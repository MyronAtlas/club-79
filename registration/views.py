from django.shortcuts import render, redirect
from registration.forms import LoginForm, SignupForm
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")
            else: form.add_error(None, "Invalid username or password")
        else:
            form = LoginForm()
        context = {"forms": form}
        return render(request, "registration/login.html", context)
    
def logout_view(request):
    logout(request)
    return redirect("login")




def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password != password_confirmation:
                form.add_error(
                    "password_confirmation", "The passwords do not match"
                )
            else:
                user = User.objects.create_user(username, password)
                login(request, user)
                return redirect("list_projects") 
    else:
        form = SignupForm()
    context = {"forms": form}
    return render(request, "registration/login.html", context)
